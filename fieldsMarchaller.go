package ctxmf

import (
	"encoding/json"
	"fmt"
)

func Sprintw(i interface{}) string {
	if i == nil {
		return "null"
	}

	switch v := i.(type) {
	case json.RawMessage:
		return string(v)
	case []byte:
		return string(v)
	default:
		return fmt.Sprint(v)
	}
}
