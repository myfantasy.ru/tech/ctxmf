package ctxmf

import (
	"context"
	"fmt"
	"runtime"
	"sync"
	"time"

	"github.com/myfantasy/ints"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

const (
	AppIDField      = "app_id"
	AppVersionField = "app_version"
	AppNameField    = "app_name"

	ContextStartField = "ctx_start"
	OperationIDField  = "operation_id"
	TraceIDField      = "trace_id"

	ErrorField        = "err"
	MethodUinionField = "method"

	MsgStart    = "start"
	MsgComplete = "complete"
	MsgError    = "error"

	Caller = "caller"
)
const (
	addCallerSkip = 4
)

var _ Context = &ContextMF{}

type ContextMF struct {
	ctx         context.Context
	startTime   time.Time
	operationID string
	logDepth    int
	paramsLast  map[string]interface{}
	paramsAll   map[string][]interface{}

	marshallerToLog MarshallerToLog
	logger          LoggerFunction

	direction string
	method    string

	metricsStartFunc MetricsStartFunction
	metricEndFunc    MetricsEndFunction

	traceID                  string
	levelOnTraceStart        Level
	levelOnTraceCompleteOK   Level
	levelOnTraceCompleteFail Level

	addCollerFlag bool

	doSow    bool
	reapList *reapListStor
}

func Background() (ctx Context) {
	return createBGCTXMF()
}

type reapListStor struct {
	reapList []ReapElement
}

func createBGCTXMF() (ctx *ContextMF) {
	ctx = &ContextMF{
		ctx:                      context.Background(),
		startTime:                time.Now(),
		operationID:              ints.DefaultUuidGenerator.Next().String(),
		paramsLast:               map[string]interface{}{},
		paramsAll:                map[string][]interface{}{},
		marshallerToLog:          DefaultMarshallerToLog,
		logDepth:                 0,
		logger:                   DefaultLoggerFunction,
		direction:                "",
		method:                   "",
		metricsStartFunc:         DefaultMetricsStartFunction,
		metricEndFunc:            DefaultMetricsEndFunction,
		traceID:                  "",
		levelOnTraceStart:        DefaultLevelOnTraceStart,
		levelOnTraceCompleteOK:   DefaultLevelOnTraceCompleteOK,
		levelOnTraceCompleteFail: DefaultLevelOnTraceCompleteFail,
		addCollerFlag:            DefaultAddColler,
	}
	return ctx
}
func (ctx *ContextMF) CopyCTXMF() *ContextMF {
	ctxOut := &ContextMF{
		ctx:                      ctx.ctx,
		startTime:                ctx.startTime,
		operationID:              ctx.operationID,
		logDepth:                 ctx.logDepth,
		paramsLast:               map[string]interface{}{},
		paramsAll:                map[string][]interface{}{},
		marshallerToLog:          ctx.marshallerToLog,
		logger:                   ctx.logger,
		direction:                ctx.direction,
		method:                   ctx.method,
		metricsStartFunc:         ctx.metricsStartFunc,
		metricEndFunc:            ctx.metricEndFunc,
		traceID:                  ctx.traceID,
		levelOnTraceStart:        ctx.levelOnTraceStart,
		levelOnTraceCompleteOK:   ctx.levelOnTraceCompleteOK,
		levelOnTraceCompleteFail: ctx.levelOnTraceCompleteFail,
		addCollerFlag:            ctx.addCollerFlag,
		doSow:                    ctx.doSow,
		reapList:                 ctx.reapList,
	}

	for k, v := range ctx.paramsLast {
		ctxOut.paramsLast[k] = v
	}
	for k, v := range ctx.paramsAll {
		ctxOut.paramsAll[k] = make([]interface{}, len(v))
		copy(ctxOut.paramsAll[k], v)
	}

	return ctxOut
}

var _ FinishProcess = &FinishProcessMF{}

type FinishProcessMF struct {
	startTime    time.Time
	ctx          *ContextMF
	mx           sync.Mutex
	span         trace.Span
	methodUinion string
	isClosed     bool
}

func (ctx *ContextMF) Deadline() (deadline time.Time, ok bool) {
	return ctx.ctx.Deadline()
}
func (ctx *ContextMF) Done() <-chan struct{} {
	return ctx.ctx.Done()
}
func (ctx *ContextMF) Err() error {
	return ctx.ctx.Err()
}
func (ctx *ContextMF) Value(key any) any {
	return ctx.ctx.Value(key)
}

func ReadStackString(skip int, depth int) string {
	res := ""
	pointers := make([]uintptr, depth)
	_ = runtime.Callers(skip, pointers)

	for i, pointer := range pointers {
		if i > 0 {
			res += "\n"
		}

		fpc := runtime.FuncForPC(pointer)
		if fpc == nil {
			continue
		}

		file, line := fpc.FileLine(pointer)
		res += fmt.Sprintf("%v:%v %v", file, line, fpc.Name())
	}

	return res
}
func (ctx *ContextMF) addCaller(skip int) *ContextMF {
	ctxOut := ctx.CopyCTXMF()

	ctxOut.With(Caller, ReadStackString(skip, 1))

	return ctxOut
}
func (ctx *ContextMF) AddCaller() (out Context) {
	return ctx.addCaller(addCallerSkip + ctx.logDepth)
}
func (ctx *ContextMF) AddCallerIfNotExists(addDepth int) (out *ContextMF) {
	if !ctx.addCollerFlag {
		return ctx
	}

	if _, ok := ctx.paramsLast[Caller]; !ok {
		ctx = ctx.CopyCTXMF().addCaller(addCallerSkip + ctx.logDepth + addDepth)
	}

	return ctx
}
func (ctx *ContextMF) AddDepth(depth int) (out Context) {
	ctxOut := ctx.CopyCTXMF()
	ctxOut.logDepth += depth
	return ctxOut
}

func (ctx *ContextMF) StartTime() time.Time {
	return ctx.startTime
}
func (ctx *ContextMF) GetOperationID() string {
	return ctx.operationID
}

// WithNoCopy modify this object
func (ctx *ContextMF) WithNoCopy(key string, value interface{}) {
	if ctx.marshallerToLog == nil {
		return
	}
	ctx.WithNoCopyObject(key, ctx.marshallerToLog(key, value))
}

// WithNoCopyObject modify this object
func (ctx *ContextMF) WithNoCopyObject(key string, value interface{}) {
	ctx.paramsLast[key] = value
	ctx.paramsAll[key] = append(ctx.paramsAll[key], value)

}

// Add param to context as string
func (ctx *ContextMF) With(key string, value interface{}) (out Context) {
	if ctx.marshallerToLog == nil {
		return ctx.Copy()
	}
	return ctx.WithObject(key, ctx.marshallerToLog(key, value))
}

// Add param to context as object
func (ctx *ContextMF) WithObject(key string, value interface{}) (out Context) {
	ctxOut := ctx.CopyCTXMF()
	ctxOut.paramsLast[key] = value
	ctxOut.paramsAll[key] = append(ctx.paramsAll[key], value)
	return ctxOut
}

func (ctx *ContextMF) GetParamsAllList() map[string][]interface{} {
	res := make(map[string][]interface{}, len(ctx.paramsAll)+6)

	for k, v := range ctx.paramsAll {
		res[k] = make([]interface{}, len(v))
		copy(res[k], v)
	}

	res[AppIDField] = append(res[AppIDField], appID)
	res[AppNameField] = append(res[AppNameField], appName)
	res[AppVersionField] = append(res[AppVersionField], appVersion)
	res[OperationIDField] = append(res[OperationIDField], ctx.operationID)
	res[ContextStartField] = append(res[ContextStartField], ctx.startTime)
	res[TraceIDField] = append(res[TraceIDField], ctx.traceID)

	return res
}

func (ctx *ContextMF) GetParamsLastList() map[string]interface{} {
	res := make(map[string]interface{}, len(ctx.paramsLast)+6)

	for k, v := range ctx.paramsLast {
		res[k] = v
	}

	res[AppIDField] = appID
	res[AppNameField] = appName
	res[AppVersionField] = appVersion
	res[OperationIDField] = ctx.operationID
	res[ContextStartField] = ctx.startTime
	res[TraceIDField] = ctx.traceID

	return res
}

func (ctx *ContextMF) Log(level Level, args ...interface{}) {
	ctx = ctx.AddCallerIfNotExists(1)
	re := ReapElement{
		Level:      level,
		Time:       time.Now(),
		Direction:  ctx.direction,
		Method:     ctx.method,
		Msg:        fmt.Sprint(args...),
		ParamsLast: ctx.GetParamsLastList(),
		ParamsAll:  ctx.GetParamsAllList(),
	}
	if ctx.doSow && ctx.reapList != nil {
		ctx.reapList.reapList = append(ctx.reapList.reapList, re)
	}
	if ctx.logger != nil {
		ctx.logger(re.Level, re.Direction, re.Method,
			re.Msg, re.ParamsLast, re.ParamsAll)
	}
}

func (ctx *ContextMF) Debug(args ...interface{}) {
	ctx.AddCallerIfNotExists(1).Log(Debug, args...)
}
func (ctx *ContextMF) Info(args ...interface{}) {
	ctx.AddCallerIfNotExists(1).Log(Info, args...)
}
func (ctx *ContextMF) Error(args ...interface{}) {
	ctx.AddCallerIfNotExists(1).Log(Error, args...)
}
func (ctx *ContextMF) Fatal(args ...interface{}) {
	ctx.AddCallerIfNotExists(1).Log(Fatal, args...)
}

func (ctx *ContextMF) SetLogger(logger LoggerFunction) (out Context) {
	ctxOut := ctx.CopyCTXMF()
	ctxOut.logger = logger
	return ctxOut
}

func (ctx *ContextMF) Start(method string) (out Context, finish FinishProcess) {
	return ctx.StartDirection("", method)
}

func (ctx *ContextMF) StartDirection(direction string, method string) (out Context, finish FinishProcess) {
	return ctx.startDirection(false, direction, method)
}

func (ctx *ContextMF) StartSowAndReapDirection(direction string, method string) (out Context, finish FinishProcess) {
	return ctx.startDirection(true, direction, method)
}

func (ctx *ContextMF) startDirection(sowAndReap bool, direction string, method string) (out Context, finish FinishProcess) {
	ctxOut := ctx.CopyCTXMF()

	if direction != "" {
		ctxOut.direction = direction
	}

	ctxOut.method = method

	methodUinion := ctxOut.method
	if ctxOut.direction != "" {
		methodUinion = ctxOut.direction + "." + methodUinion
	}

	ctxOut.WithNoCopyObject(MethodUinionField, methodUinion)

	if ctx.metricsStartFunc != nil {
		ctx.metricsStartFunc(ctxOut, ctxOut.direction, ctxOut.method)
	}

	ctxTel, span := otel.Tracer(appName).Start(ctxOut.ctx, methodUinion)
	ctxOut.traceID = span.SpanContext().TraceID().String()
	ctxOut.ctx = ctxTel
	if sowAndReap && !ctxOut.doSow {
		ctxOut.doSow = true
		ctxOut.reapList = &reapListStor{}
	}

	fp := &FinishProcessMF{
		startTime:    time.Now(),
		ctx:          ctxOut,
		span:         span,
		methodUinion: methodUinion,
	}
	ctxOut.AddDepth(1).Log(ctxOut.levelOnTraceStart, MsgStart)

	return ctxOut, fp
}

func (ctx *ContextMF) GetTraceID() string {
	return ctx.traceID
}

func (ctx *ContextMF) SetMetricsFuncs(startFunc MetricsStartFunction, endFunc MetricsEndFunction) (out Context) {
	ctxOut := ctx.CopyCTXMF()
	ctxOut.metricsStartFunc = startFunc
	ctxOut.metricEndFunc = endFunc
	return ctxOut
}

func (ctx *ContextMF) SetTraceLogLevels(onTraceStart Level, onTraceCompleteOK Level, onTraceCompleteFail Level) (out Context) {
	ctxOut := ctx.CopyCTXMF()
	ctxOut.levelOnTraceStart = onTraceStart
	ctxOut.levelOnTraceCompleteOK = onTraceCompleteOK
	ctxOut.levelOnTraceCompleteFail = onTraceCompleteFail
	return ctxOut
}

func (ctx *ContextMF) Restart() (out Context) {
	ctxOut := ctx.CopyCTXMF()
	ctxOut.startTime = time.Now()
	ctxOut.operationID = ints.DefaultUuidGenerator.Next().String()
	return ctxOut
}
func (ctx *ContextMF) SetContext(ctxSet context.Context) (out Context) {
	ctxOut := ctx.CopyCTXMF()
	ctxOut.ctx = ctxSet
	return ctxOut
}
func (ctx *ContextMF) Copy() (out Context) {
	ctxOut := ctx.CopyCTXMF()
	return ctxOut
}

func (ctx *ContextMF) WithDeadline(d time.Time) (out Context, cancel context.CancelFunc) {
	ctxOut := ctx.CopyCTXMF()
	ctxOut.ctx, cancel = context.WithDeadline(ctx, d)
	return ctxOut, cancel
}
func (ctx *ContextMF) WithTimeout(timeout time.Duration) (out Context, cancel context.CancelFunc) {
	ctxOut := ctx.CopyCTXMF()
	ctxOut.ctx, cancel = context.WithTimeout(ctx, timeout)
	return ctxOut, cancel
}
func (ctx *ContextMF) WithCancel() (out Context, cancel context.CancelFunc) {
	ctxOut := ctx.CopyCTXMF()
	ctxOut.ctx, cancel = context.WithCancel(ctx)
	return ctxOut, cancel
}
func (ctx *ContextMF) WithError(err error) (out Context) {
	return ctx.With(ErrorField, err)
}

func (fp *FinishProcessMF) With(key string, value interface{}) FinishProcess {
	fp.mx.Lock()
	fp.ctx.WithNoCopy(key, value)
	fp.mx.Unlock()
	return fp
}
func (fp *FinishProcessMF) WithObject(key string, value interface{}) FinishProcess {
	fp.mx.Lock()
	fp.ctx.WithNoCopyObject(key, value)
	fp.mx.Unlock()
	return fp
}
func (fp *FinishProcessMF) Complete(err error) error {
	fp.mx.Lock()

	if !fp.isClosed {
		if err != nil {
			fp.ctx.WithNoCopy(ErrorField, err)
		}

		DefaultFillSpanParamsFunction(fp.span, fp.ctx.direction, fp.ctx.method, err,
			fp.ctx.GetParamsLastList(), fp.ctx.GetParamsAllList())

		if err != nil {
			fp.span.SetAttributes(attribute.String("operationResult", MsgError))
			fp.span.SetStatus(codes.Error, fp.methodUinion+" FAIL")
			fp.span.RecordError(err)

			if fp.ctx.metricEndFunc != nil {
				fp.ctx.metricEndFunc(fp.ctx, fp.startTime, fp.ctx.direction, fp.ctx.method, MsgError, err)
			}
		} else {
			fp.span.SetAttributes(attribute.String("operationResult", MsgComplete))

			if fp.ctx.metricEndFunc != nil {
				fp.ctx.metricEndFunc(fp.ctx, fp.startTime, fp.ctx.direction, fp.ctx.method, MsgComplete, err)
			}
		}

		fp.span.End()

		fp.isClosed = true
	}

	if err == nil {
		fp.ctx.AddDepth(1).Log(fp.ctx.levelOnTraceCompleteOK, MsgComplete)
	} else {
		fp.ctx.AddDepth(1).Log(fp.ctx.levelOnTraceCompleteFail, MsgError)
	}
	fp.mx.Unlock()
	return err
}
func (fp *FinishProcessMF) Reap() []ReapElement {
	if fp.ctx.reapList == nil {
		return nil
	}
	return fp.ctx.reapList.reapList
}

func (fp *FinishProcessMF) Context() Context {
	return fp.ctx
}
