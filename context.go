package ctxmf

import (
	"context"
	"time"

	"go.opentelemetry.io/otel/trace"
)

type Level string

const (
	Debug Level = "debug"
	Info  Level = "info"
	Error Level = "error"
	Fatal Level = "fatal"
)

// LoggerFunction writes log
// paramsLast - Last value of params
// paramsAll - All params value
type LoggerFunction func(level Level, direction string, method string, msg string,
	paramsLast map[string]interface{}, paramsAll map[string][]interface{})

type ReapElement struct {
	Level      Level
	Time       time.Time
	Direction  string
	Method     string
	Msg        string
	ParamsLast map[string]interface{}
	ParamsAll  map[string][]interface{}
}

// FillSpanParamsFunction writes intoSpan params
// paramsLast - Last value of params
// paramsAll - All params value
type FillSpanParamsFunction func(span trace.Span, direction string, method string,
	err error,
	paramsLast map[string]interface{}, paramsAll map[string][]interface{})

type MetricsStartFunction func(ctx context.Context, direction string, method string)
type MetricsEndFunction func(ctx context.Context, startRequest time.Time, direction string, method string, resultName string, err error)

type MarshallerToLog func(key string, value interface{}) interface{}

type FinishProcess interface {
	// Add param to context as string (or use other marchaller)
	With(key string, value interface{}) FinishProcess
	// Add param to context as object
	WithObject(key string, value interface{}) FinishProcess
	Complete(err error) error
	Context() Context
	Reap() []ReapElement
}

type Logger interface {
	AddCaller() (out Context)
	AddDepth(depth int) (out Context)

	// StartTime time when context starts or restarts
	StartTime() time.Time
	// GetOperationID context chain unique id (copies from parent to child)
	GetOperationID() string
	// Restart context - refresh start time and operation id
	Restart() (out Context)

	// Add param to context as string (or use other marchaller)
	With(key string, value interface{}) (out Context)
	// Add param to context as object
	WithObject(key string, value interface{}) (out Context)

	Log(level Level, args ...interface{})
	Debug(args ...interface{})
	Info(args ...interface{})
	Error(args ...interface{})
	Fatal(args ...interface{})

	SetLogger(logger LoggerFunction) (out Context)
}

type Tracer interface {
	Start(method string) (out Context, finish FinishProcess)
	StartDirection(direction string, method string) (out Context, finish FinishProcess)
	StartSowAndReapDirection(direction string, method string) (out Context, finish FinishProcess)
	GetTraceID() string

	SetMetricsFuncs(startFunc MetricsStartFunction, endFunc MetricsEndFunction) (out Context)
	SetTraceLogLevels(onTraceStart Level, onTraceCompleteOK Level, onTraceCompleteFail Level) (out Context)
}

type Context interface {
	Logger
	Tracer

	context.Context

	SetContext(ctx context.Context) (out Context)
	Copy() (out Context)

	WithDeadline(d time.Time) (out Context, cancel context.CancelFunc)
	WithTimeout(timeout time.Duration) (out Context, cancel context.CancelFunc)
	WithCancel() (out Context, cancel context.CancelFunc)
	WithError(err error) (out Context)
}
