# ctxmf

Context with start and stop trace

## jsonify
A set of methods for representing an object as json  
The generated json will contain private fields  
Example:
```Golang
type formatTest struct {
	a int
	B int `jignore:""`
	c int `masker:""`
	d int
	E string
	G string `masker:"fl"`
}

func main() {
    fmt.Println(jsonify.JsonifySLn(&formatTest{0, 1, 2234324, 3, "4", "2346876786"}))
}
/*
{
 "a": 0,
 "c": "*******",
 "d": 3,
 "E": "4",
 "G": "2********6"
}
*/


```
