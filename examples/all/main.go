package main

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/myfantasy.ru/tech/ctxmf"
	"gitlab.com/myfantasy.ru/tech/ctxmf/loggers/jsonify"
	"gitlab.com/myfantasy.ru/tech/ctxmf/loggers/simpleConsole"
	"gitlab.com/myfantasy.ru/tech/ctxmf/tracerInit"
)

func main() {
	initLogger()
	deferFunc := initTracer()
	defer deferFunc()
	initMetrics()

	ctx := ctxmf.Background()

	ctx, finish := ctx.Start("test1")
	_, finish2 := ctx.StartSowAndReapDirection("DIR1", "test2")
	_ = finish2.Complete(nil)

	fmt.Println(string(jsonify.Jsonify(finish2.Reap(), " ", "\n")))

	finish.With("a", 6).With("b", 7)

	_ = finish.Complete(nil)
}

func initLogger() {
	logger := simpleConsole.ConsoleLogger{
		Format: simpleConsole.JsonFormatWithIndent,
	}
	ctxmf.DefaultLoggerFunction = logger.LoggerFunction
}

func initTracer() func() {
	tp, err := tracerInit.NewConsoleTracer()
	if err != nil {
		panic(err)
	}

	return func() {
		if err := tp.Shutdown(context.Background()); err != nil {
			log.Printf("Error shutting down tracer provider: %v", err)
		}
	}
}

func initMetrics() {
	ctxmf.InitMetrics()
}
