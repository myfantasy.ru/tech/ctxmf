package main

import (
	"gitlab.com/myfantasy.ru/tech/ctxmf"
	"gitlab.com/myfantasy.ru/tech/ctxmf/loggers/simpleConsole"
)

func main() {
	initLogger()

	ctx := ctxmf.Background()

	ctx, finish := ctx.Start("test1")
	ctx.StartDirection("DIR1", "test2")

	finish.With("a", 6).With("b", 7)

	_ = finish.Complete(nil)
}

func initLogger() {
	logger := simpleConsole.ConsoleLogger{
		Format: simpleConsole.JsonFormatWithIndent,
	}
	ctxmf.DefaultLoggerFunction = logger.LoggerFunction
}
