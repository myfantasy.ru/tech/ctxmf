package main

import (
	"errors"
	"fmt"

	"gitlab.com/myfantasy.ru/tech/ctxmf"
	"gitlab.com/myfantasy.ru/tech/ctxmf/loggers/jsonify"
)

type Jsnfbl struct {
}

func (*Jsnfbl) Jsonify() []byte {
	return []byte("ttest Jsonify")
}

type formatTest struct {
	a int
	B int `jignore:""`
	c int `masker:""`
	d int
	E string
	G string `masker:"fl"`
}

func main() {
	fmt.Println(jsonify.JsonifySLn(fmt.Errorf("ABC %v", 123)))

	fmt.Println(jsonify.JsonifySLn(fmt.Errorf("abc %w", errors.New("abc"))))

	fmt.Println(jsonify.JsonifySLn(jsonify.GetSpecMarchaller()))

	fmt.Println(jsonify.JsonifySLn(&Jsnfbl{}))

	fmt.Println(jsonify.JsonifySLn(&formatTest{0, 1, 2234324, 3, "4", "2346876786"}))

	ctx := ctxmf.Background()

	ctx, finish := ctx.Start("test1")
	ctx, finish2 := ctx.StartSowAndReapDirection("DIR1", "test2")

	finish.With("a", 6).With("b", 7)
	finish2.With("a2", 6).With("b2", 7)
	_, finish3 := ctx.StartSowAndReapDirection("DIR2", "test3")
	finish3.With("a3", 6).With("b3", 7)
	_ = finish3.Complete(nil)
	_ = finish2.Complete(errors.New("finish2 fail abc"))
	_ = finish.Complete(nil)

	fmt.Println(string(jsonify.Jsonify(finish2.Reap(), " ", "\n")))
}
