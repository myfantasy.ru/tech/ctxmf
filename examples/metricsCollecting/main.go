package main

import "gitlab.com/myfantasy.ru/tech/ctxmf"

func main() {
	ctxmf.InitMetrics()

	ctx := ctxmf.Background()

	ctx, finish := ctx.Start("test1")
	ctx.StartDirection("DIR1", "test2")

	finish.With("a", 6).With("b", 7)

	_ = finish.Complete(nil)
}
