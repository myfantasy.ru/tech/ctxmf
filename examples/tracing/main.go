package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"gitlab.com/myfantasy.ru/tech/ctxmf"
	"gitlab.com/myfantasy.ru/tech/ctxmf/tracerInit"
)

func main() {
	tp, err := tracerInit.NewConsoleTracer()
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := tp.Shutdown(context.Background()); err != nil {
			log.Printf("Error shutting down tracer provider: %v", err)
		}
	}()

	exampleTrace()

	time.Sleep(time.Second)
}

func exampleTrace() {
	ctxmf.SetAppName("testAppName")

	ctx := ctxmf.Background().With("test", "trace").
		With("state", "abc")
	ctx2, complete2 := ctx.StartDirection("test_dir", "start.test")
	_, complete3 := ctx2.Start("start.test2")
	complete3.Complete(nil)
	_, complete4 := ctx2.Start("start.test3")
	complete4.Complete(fmt.Errorf("test error4"))
	complete2.Complete(nil)

}
