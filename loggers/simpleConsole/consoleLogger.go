package simpleConsole

import (
	"fmt"
	"sort"
	"time"

	"gitlab.com/myfantasy.ru/tech/ctxmf"
	"gitlab.com/myfantasy.ru/tech/ctxmf/loggers/jsonify"
)

type LogFormat string

const (
	JsonFormat           LogFormat = ""
	JsonFormatWithIndent LogFormat = "jsonIndent"
)

type ConsoleLogger struct {
	Format LogFormat
}

func (cl *ConsoleLogger) LoggerFunction(level ctxmf.Level, direction string, method string, msg string,
	paramsLast map[string]interface{}, paramsAll map[string][]interface{}) {

	rows := make([]logRow, len(paramsAll)+4)

	rows[0] = logRow{"level", level, 0}
	rows[1] = logRow{"ts", cl.getTimeStamp(), 1}
	if direction != "" {
		method = direction + "." + method
	}
	rows[2] = logRow{"method", method, 2}
	rows[3] = logRow{"msg", msg, 3}

	i := 3
	for k, v := range paramsAll {
		i++
		if len(v) == 1 {
			rows[i] = logRow{k, v[0], cl.namePriority(k)}
		} else {
			rows[i] = logRow{k, v, cl.namePriority(k)}
		}
	}

	cl.write(rows)
}

func (cl *ConsoleLogger) getTimeStamp() interface{} {
	return time.Now().Format(time.RFC3339Nano)
}
func (cl *ConsoleLogger) namePriority(name string) int {
	if name == ctxmf.ErrorField {
		return 4
	}
	return 5
}

func (cl *ConsoleLogger) write(rows []logRow) {
	sort.Slice(rows, func(i, j int) bool {
		return rows[i].order < rows[j].order ||
			(rows[i].order == rows[j].order && rows[i].name < rows[j].name)
	})

	if cl.Format == JsonFormat || cl.Format == JsonFormatWithIndent {
		cl.writeJson(rows)
	}
}

func (cl *ConsoleLogger) writeJson(rows []logRow) {
	res := []byte{}
	res = append(res, []byte("{")...)

	for i, row := range rows {
		if i > 0 {
			res = append(res, []byte(",")...)
		}
		res = append(res, []byte("\"")...)
		res = append(res, []byte(ctxmf.Sprintw(row.name))...)
		res = append(res, []byte("\":")...)
		res = append(res, jsonify.Jsonify(row.value, "", "")...)
		res = append(res, []byte("")...)
	}

	res = append(res, []byte("}")...)

	fmt.Println(string(res))
}

type logRow struct {
	name  string
	value interface{}
	order int
}
