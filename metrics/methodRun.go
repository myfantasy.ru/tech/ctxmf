package metrics

import (
	"context"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

type MethodRun struct {
	Start           *prometheus.CounterVec
	FinishTotal     *prometheus.CounterVec
	FinishTimeHist  *prometheus.HistogramVec
	FinishTimeTotal *prometheus.CounterVec
}

func NewMethodRun(labels map[string]string) *MethodRun {
	constLabels := prometheus.Labels(labels)

	return &MethodRun{
		Start: prometheus.NewCounterVec(prometheus.CounterOpts{
			Name:        "method_run_start",
			Help:        "Total amount of runnings",
			ConstLabels: constLabels,
		}, []string{"direction_name", "method_name"}),
		FinishTotal: prometheus.NewCounterVec(prometheus.CounterOpts{
			Name:        "method_run_finish",
			Help:        "How many HTTP runnings finish, partitioned",
			ConstLabels: constLabels,
		}, []string{
			"direction_name", "method_name", "status_code",
		}),
		FinishTimeHist: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Name:        "method_run_finish_hist",
			Help:        "Total amount of time spent on the runnings finish",
			ConstLabels: constLabels,
			Buckets:     []float64{5, 10, 20, 50, 100, 200, 500, 1000, 2000},
		}, []string{
			"direction_name", "method_name", "status_code",
		}),
		FinishTimeTotal: prometheus.NewCounterVec(prometheus.CounterOpts{
			Name:        "method_run_finish_time_total",
			Help:        "Total amount of time spent on the runnings finish",
			ConstLabels: constLabels,
		}, []string{
			"direction_name", "method_name", "status_code",
		}),
	}
}

func (m *MethodRun) AutoRegister() *MethodRun {
	m.MustRegister(prometheus.DefaultRegisterer)

	return m
}

func (m *MethodRun) MustRegister(registerer prometheus.Registerer) {
	registerer.MustRegister(
		m.Start,
		m.FinishTotal,
		m.FinishTimeHist,
		m.FinishTimeTotal,
	)
}

func (m *MethodRun) WriteMetricRequest(_ context.Context,
	direction string, method string,
) {
	if m == nil {
		return
	}
	m.Start.WithLabelValues(direction, method).Inc()
}

func (m *MethodRun) WriteMetricResponse(_ context.Context, mRequest time.Time,
	direction string, method string, resultName string,
	_ error,
) {
	if m == nil {
		return
	}

	responseLabels := []string{direction, method, resultName}

	m.FinishTotal.WithLabelValues(responseLabels...).Inc()
	diff := time.Since(mRequest).Milliseconds()
	m.FinishTimeHist.WithLabelValues(responseLabels...).Observe(float64(diff))
	m.FinishTimeTotal.WithLabelValues(responseLabels...).Add(float64(diff))
}
