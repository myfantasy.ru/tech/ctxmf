package ctxmf

import (
	"fmt"

	"github.com/myfantasy/ints"
	"gitlab.com/myfantasy.ru/tech/ctxmf/metrics"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

var appID = ints.DefaultUuidGenerator.Next().String()
var appName string = "app_name"
var appVersion string = "v0.0.0-dev"

func SetAppName(name string) {
	appName = name
}
func GetAppName() (name string) {
	return name
}

func SetAppVersion(version string) {
	appVersion = version
}
func GetAppVersion() (version string) {
	return appVersion
}

func GetAppID() string {
	return appID
}

var DefaultAddColler bool = true

var DefaultMarshallerToLog MarshallerToLog = func(key string, value interface{}) interface{} {
	return fmt.Sprintf("%v", value)
}
var DefaultLoggerFunction LoggerFunction
var DefaultMetricsStartFunction MetricsStartFunction
var DefaultMetricsEndFunction MetricsEndFunction

func InitMetrics() {
	labels := map[string]string{"version": appVersion, "service": appName}
	m := metrics.NewMethodRun(labels).AutoRegister()
	DefaultMetricsStartFunction = m.WriteMetricRequest
	DefaultMetricsEndFunction = m.WriteMetricResponse
}

var DefaultLevelOnTraceStart Level = Debug
var DefaultLevelOnTraceCompleteOK Level = Info
var DefaultLevelOnTraceCompleteFail Level = Error

var DefaultFillSpanParamsFunction FillSpanParamsFunction = FillSpanParamsFunctionWide

func FillSpanParamsFunctionWide(span trace.Span, direction string, method string,
	err error,
	paramsLast map[string]interface{}, paramsAll map[string][]interface{}) {

	for k, v := range paramsAll {
		if len(v) == 1 {
			span.SetAttributes(attribute.String(k, Sprintw(v[0])))
		} else {
			span.SetAttributes(attribute.String(k, Sprintw(v)))
		}
	}
}
